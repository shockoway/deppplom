package com.kamilla.deppplom.users;

public enum Role {

    ADMIN,
    TEACHER,
    STUDENT

}
